# Notification Project

### Project Setup

<b>Create Database:</b>

- create role db_user with login password 'your_password';
- create database db_name with owner db_user;
  
<b>Django Setup</b>:

- git clone the project
- python -m venv venv
- source venv/bin/activate
- source .env
- pip install -r requirements.txt
- python manage.py migrate
